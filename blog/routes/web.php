<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/tabel', function () {
    return view('tabel');
});


Route::get('/data_tabel', function () {
    return view('data_tabel');
});

Route::get('/master', function () {
    return view('master');
});