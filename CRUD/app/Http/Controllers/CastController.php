<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PhpParser\Node\Expr\Cast;

class CastController extends Controller
{
    
    public function index()
    {
        $cast = Cast::all();
        return view('cast.index', compact('cast'));
    }

    public function create()
    {
        return view('cast.create');
    }
    public function store(Request $request)
    {

         $validatedData = request()->validate([
            'name' => ['required','min:3'],
            'umur' => 'required',
            'bio' => 'required'
        ]);


        Cast::create($validatedData);

        return redirect('/cast');
    }

    public function show(Cast $cast)
    {
        return view('cast.show', compact('cast'));
    }

    public function edit(Cast $cast)
    {
        return view('cast.edit', compact('cast'));
    }

    public function update(Request $request, Cast $cast)
    {
        $validatedData = request()->validate([
            'name' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        ]);

        $cast->update($validatedData);

        return redirect('/cast');
    }

    public function destroy(Cast $cast)
    {
        $cast->delete();
        return redirect('/cast');
    }
}
