@extends('layout')

@section('title')
List cast
@endsection
@section('mainContent')
	<h1>List Car</h1>

	<table class="table table-striped">
		<thead>
			<tr>
				<th>Name</th>
			</tr>
		</thead>
		<tbody>
		
		<tr>
			<td><a href="{{$cast->id}}">{{$cast->name}}</a> </td>
		</tr>
	
		</tbody>
	</table>
	<hr>
	<a href="cast/create">Add New Car</a>
@endsection
