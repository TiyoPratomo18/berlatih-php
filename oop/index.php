<?php

require_once("animal.php");
require_once("ape.php");
require_once("frog.php");

$sheep = new animal("shaun");
echo "Name = " . $sheep->name. "<br>"; // "shaun"
echo "Legs = " . $sheep->legs. "<br>"; // 4
echo "Cold Blooded = " . $sheep->cold_blooded; // "no"

echo "<br><br>";

$kodok = new Frog("buduk");
echo "Name = " . $kodok->name. "<br>"; // "shaun"
echo "Legs = " . $kodok->legs. "<br>"; // 4
echo "Cold Blooded = " . $kodok->cold_blooded. "<br>"; // "no"
$kodok->jump(); // "hop hop"

echo "<br><br>";

$sungokong = new ape("kera sakti");
echo "Name = " . $sungokong->name. "<br>"; // "shaun"
echo "Legs = " . $sungokong->legs. "<br>"; // 4
echo "Cold Blooded = " . $sungokong->cold_blooded. "<br>"; // "no"
$sungokong->yell(); // "Auooo"

?>